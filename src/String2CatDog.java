package src;

// String-2 > catDog
// https://codingbat.com/prob/p111624

public class String2CatDog {
    public boolean catDog(String str) {
        int cat = 0;
        int dog = 0;
        for(int i = 0; i < str.length() - 2; i++) {
            if (str.substring(i, i+3).equals("cat")) cat++;
            if (str.substring(i, i+3).equals("dog")) dog++;
        }
        return cat == dog;
    }
}
