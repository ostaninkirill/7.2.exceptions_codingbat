package src;

// Array-2 > sum13
// https://codingbat.com/prob/p127384

public class Array2Sum13 {
    public int sum13(int[] nums) {
        if (nums.length == 0) return 0;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 13) sum += nums[i];
            else {
                i++;
            }
        }
        return sum;
    }
}
