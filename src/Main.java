package src;

public class Main {

    public static void main(String[] args) {
        System.out.println("Первая задача");
	    Logic2MakeChocolate logic2MakeChocolate = new Logic2MakeChocolate();
        System.out.println(logic2MakeChocolate.makeChocolate(4,1,9));
        System.out.println(logic2MakeChocolate.makeChocolate(4,1,10));
        System.out.println(logic2MakeChocolate.makeChocolate(4,1,7));

        System.out.println();
        System.out.println("Вторая задача");
        String2CatDog string2CatDog = new String2CatDog();
        System.out.println(string2CatDog.catDog("catdog"));
        System.out.println(string2CatDog.catDog("catcat"));
        System.out.println(string2CatDog.catDog("1cat1cadodog"));

        System.out.println();
        System.out.println("Третья задача");
        Array2Sum13 array2Sum13 = new Array2Sum13();
        System.out.println(array2Sum13.sum13(new int[] {1, 2, 2, 1}));
        System.out.println(array2Sum13.sum13(new int[] {1, 1}));
        System.out.println(array2Sum13.sum13(new int[] {1, 2, 2, 1, 13}));

        System.out.println();
        System.out.println("Четвертая задача");
        Map2WordCount map2WordCount = new Map2WordCount();
        System.out.println(map2WordCount.wordCount(new String[] {"a", "b", "a", "c", "b"}));
        System.out.println(map2WordCount.wordCount(new String[] {"c", "b", "a"}));
        System.out.println(map2WordCount.wordCount(new String[] {"c", "c", "c", "c"}));

    }
}
